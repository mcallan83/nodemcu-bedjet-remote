#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <FS.h>
#include <WiFiManager.h>
#include <string>

MDNSResponder mdns;
ESP8266WebServer server (80);

const int TX_PIN = D1;
const char* host = "bedjet";

// button codes - 3 for each button
const char* COOL_0        = "1111110011010001101101101000001000001000";
const char* COOL_1        = "1111110011010001101101100001110110100011";
const char* COOL_2        = "1111110011010001101101100001110010100010";
const char* DOWN_0        = "1111101011010001101101100011110011000000";
const char* DOWN_1        = "1111101011010001101101100000110010010000";
const char* DOWN_2        = "1111101011010001101101100000101110001111";
const char* HEAT_0        = "1111110111010001101101101100110001010011";
const char* HEAT_1        = "1111110111010001101101101100101101010010";
const char* HEAT_2        = "1111110111010001101101101100101001010001";
const char* MUTE_0        = "1111100111010001101101101011110101000000";
const char* MUTE_1        = "1111100111010001101101101011110000111111";
const char* MUTE_2        = "1111100111010001101101101011101100111110";
const char* TIMER_0       = "1111100011010001101101101011101000111100";
const char* TIMER_1       = "1111100011010001101101101011100100111011";
const char* TIMER_2       = "1111100011010001101101101011100000111010";
const char* TURBO_HEAT_0  = "1111111011010001101101101100000001001000";
const char* TURBO_HEAT_1  = "1111111011010001101101101011111101000111";
const char* TURBO_HEAT_2  = "1111111011010001101101101011111001000110";
const char* UP_0          = "1111101111010001101101101100111101010100";
const char* UP_1          = "1111101111010001101101101100111001010011";
const char* UP_2          = "1111101111010001101101101100110101010010";


//button state counters
int COOL = 0;
int DOWN = 0;
int HEAT = 0;
int MUTE = 0;
int TIMER = 0;
int TURBO_HEAT = 0;
int UP = 0;

//signal types
typedef struct  {
  boolean power;
  int duration;
} signal;

const signal SHORT_ON { false, 406.5};
const signal LONG_ON {false, 1168};
const signal OFF {true, 656};


void cool (void) {
  switch (COOL) {
    case 0:
      doTransmission(COOL_0);
      COOL++;
      break;
    case 1:
      doTransmission(COOL_1);
      COOL++;
      break;
    case 2:
      doTransmission(COOL_2);
      COOL = 0;
  }
}


void down (void) {
  switch (DOWN) {
    case 0:
      doTransmission(DOWN_0);
      DOWN++;
      break;
    case 1:
      doTransmission(DOWN_1);
      DOWN++;
      break;
    case 2:
      doTransmission(DOWN_2);
      DOWN = 0;
  }
}

void heat (void) {
  switch (HEAT) {
    case 0:
      doTransmission(HEAT_0);
      HEAT++;
      break;
    case 1:
      doTransmission(HEAT_1);
      HEAT++;
      break;
    case 2:
      doTransmission(HEAT_2);
      HEAT = 0;
  }
}

void mute (void) {
  switch (MUTE) {
    case 0:
      doTransmission(MUTE_0);
      MUTE++;
      break;
    case 1:
      doTransmission(MUTE_1);
      MUTE++;
      break;
    case 2:
      doTransmission(MUTE_2);
      MUTE = 0;
  }
}

void timer (void) {
  switch (TIMER) {
    case 0:
      doTransmission(TIMER_0);
      TIMER++;
      break;
    case 1:
      doTransmission(TIMER_1);
      TIMER++;
      break;
    case 2:
      doTransmission(TIMER_2);
      TIMER = 0;
  }
}

void turboHeat (void) {
  switch (TURBO_HEAT) {
    case 0:
      doTransmission(TURBO_HEAT_0);
      TURBO_HEAT++;
      break;
    case 1:
      doTransmission(TURBO_HEAT_1);
      TURBO_HEAT++;
      break;
    case 2:
      doTransmission(TURBO_HEAT_2);
      TURBO_HEAT = 0;
  }
}

void up (void) {
  switch (UP) {
    case 0:
      doTransmission(UP_0);
      UP++;
      break;
    case 1:
      doTransmission(UP_1);
      UP++;
      break;
    case 2:
      doTransmission(UP_2);
      UP = 0;
  }
}








void setup ( void ) {
  pinMode(TX_PIN, OUTPUT);
  Serial.begin ( 115200 );
  Serial.println ( "Initializing" );
  WiFiManager wifiManager;
  wifiManager.autoConnect(host, "esp8266");

  if ( mdns.begin ( host, WiFi.localIP() ) ) {
    Serial.println ( "MDNS responder started" );
  }

  bool result = SPIFFS.begin();
  if (result != true) {
    SPIFFS.format();
  }
  Serial.println("SPIFFS opened: " + result);
  server.on("/control", HTTP_ANY, commandSet);
  server.onNotFound ( handleNotFound );
  server.begin();
  Serial.println ( "HTTP server started" );
}

void doTransmission(char const* source) {

  Serial.println("Transmitting...");

  // pre signal
  digitalWrite(TX_PIN, true);
  delayMicroseconds (52554);


  // bursts
  for (int burst = 1; burst < 8; burst++) {
    //
    digitalWrite(TX_PIN, false);
    delayMicroseconds (4776);

    digitalWrite(TX_PIN, true);
    delayMicroseconds (207);

    for (int i = 0; i < strlen(source); i++) {
      if (source[i] == '1') {
        digitalWrite(TX_PIN, SHORT_ON.power);
        delayMicroseconds (SHORT_ON.duration);
      } else {
        digitalWrite(TX_PIN, LONG_ON.power);
        delayMicroseconds (LONG_ON.duration);
      }
      digitalWrite(TX_PIN, OFF.power);
      delayMicroseconds (OFF.duration);
    }

  }

  digitalWrite(TX_PIN, true);
  delayMicroseconds (104434);
  digitalWrite(TX_PIN, false);
}

void commandSet() {
  String action = server.arg("action");
  Serial.print("Action: ");
  Serial.println(action);

  if (action == "cool") {
    cool();
  } else if (action == "down") {
    down();
  } else if (action == "heat") {
    heat();
  } else if (action == "mute") {
    mute();
  } else if (action == "timer") {
    timer();
  } else if (action == "turbo_heat") {
    turboHeat();
  } else if (action == "up") {
    up();
  }

  server.send (200, "text/plain", "");
}

void handleNotFound() {
  if (!handleFileRead(server.uri())) {
    server.send(404, "text/plain", "FileNotFound");
  }
}

bool handleFileRead(String path) {
  Serial.println("handleFileRead: " + path);
  if (path.endsWith("/")) path += "index.html";
  String contentType = "text/html";
  if (SPIFFS.exists(path)) {
    File file = SPIFFS.open(path, "r");
    size_t sent = server.streamFile(file, contentType);
    file.close();
    return true;
  }
  return false;
}


void loop ( void ) {
  mdns.update();
  server.handleClient();
}



